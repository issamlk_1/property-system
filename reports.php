<?php 

/*
	this function is for content of report page
 */

function myplguin_admin_page()
{
?>
<!-- title of page -->
<div class="warp">
<h1>صفحة الاحصائيات</h1>


<?php
/*
get All rents
 */
 $args  = array('post_type'=>'Rent');
 $rents =  get_posts($args);

/*
loop throw rents to get data from repeaters
 */
foreach ( $rents as $rent ) : setup_postdata( $rent );
		if( have_rows('new_payment',$rent->ID)):
			 while( have_rows('new_payment',$rent->ID) ): the_row(); 
				// vars
				$values[] = array(
					'value' =>	get_sub_field('value'),
					'date' =>	get_sub_field('date'),
					);
			 endwhile;	
		endif;
endforeach;

/*
set values dates parameters
 */
	$values = array_merge($values); // for merge the arrays inside values array
	$this_month = date("m"); //get this month
/*
sum the value of rents
 */
	$sum_rent = 0;
	foreach ($values as $value) {
		$pieces = explode("/", $value['date']);
		if($pieces[1] == $this_month){
			$sum_rent += $value['value'];
		}
}?>
<h2>مجموع ايرادات الإيجار لهذا الشهر هو :</h2><h3><?php echo $sum_rent; ?></h3>
<br>


<?php
/*
get All rents
 */
 $args  = array('post_type'=>'Sell');
 $sells =  get_posts($args);

/*
loop throw rents to get data from repeaters
 */
foreach ( $sells as $sell ) : setup_postdata( $sell );
				// vars
				$sell_values[] = array(
					'value' =>	get_field('price',$sell->ID),
					'date' =>	get_field('date',$sell->ID),
					'comition' =>	get_field('comition',$sell->ID),
					);
endforeach;

/*
set values dates parameters
 */
	$sell_values = array_merge($sell_values); // for merge the arrays inside values array
	$this_month = date("m"); //get this month
/*
sum the value of rents
 */
	$sum_sell = 0;
	$sum_comition = 0;
	foreach ($sell_values as $value) {
		$pieces = explode("/", $value['date']);
		if($pieces[1] == $this_month){
			$sum_sell += $value['value'];
			$sum_comition += $value['comition'];
		}
}?>
<h2>مجموع المبيعات لهذا الشهر هو :</h2><h3><?php echo $sum_sell; ?></h3>
<br>
<h2>مجموع ايرادات المبيعات لهذا الشهر هو :</h2><h3><?php echo $sum_comition; ?></h3>
<br>
<?php
/*
get All rents
 */
 $args  = array('post_type'=>'Payment');
 $pays =  get_posts($args);

/*
loop throw rents to get data from repeaters
 */
foreach ( $pays as $pay ) : setup_postdata( $pay );
		if( have_rows('pays',$pay->ID)):
			 while( have_rows('pays',$pay->ID) ): the_row(); 
				// vars
				$pays_values[] = array(
					'value' =>	get_sub_field('value'),
					'date' =>	get_sub_field('date'),
					);
			 endwhile;	
		endif;
endforeach;

/*
set values dates parameters
 */
	$pays_values = array_merge($pays_values); // for merge the arrays inside values array
	$this_month = date("m"); //get this month
/*
sum the value of rents
 */
	$sum_pay = 0;
	foreach ($pays_values as $value) {
		$pieces = explode("/", $value['date']);
		if($pieces[1] == $this_month){
			$sum_pay += $value['value'];
		}
}?>
<h2>مجموع  المصاريف لهذا الشهر هو :</h2><h3><?php echo $sum_pay;  ?></h3>
<br>
<br>
<br>
<h2>الربح الصافي هو :</h2><h3><?php echo $sum_rent+$sum_comition-$sum_pay; ?></h3>
<br>

<h3>تاريخ اليوم</h3>
<?php
echo date("d-m-Y");
wp_reset_postdata();
 ?>

</div>
<?php
}
