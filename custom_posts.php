<?php
/**
* 	custom posts 			
*/
class rent_custom_posts 
{
	
	function __construct()
	{
		add_action( 'init', [$this,'rent_register_rent']);
		add_action( 'init', [$this,'rent_register_sell']);
		add_action( 'init', [$this,'rent_register_pay']);

	}



	

	/**
	* Registers a new post type
	* @uses $wp_post_types Inserts new post type object into the list
	*
	* @param string  Post type key, must not exceed 20 characters
	* @param array|string  See optional args description above.
	* @return object|WP_Error the registered post type object, or an error object
	*/
	function rent_register_rent() {
	
		$labels = array(
			'name'                => __( 'Rent', 'text-domain' ),
			'singular_name'       => __( 'Rent', 'text-domain' ),
			'add_new'             => _x( 'Add New Rent', 'text-domain', 'text-domain' ),
			'add_new_item'        => __( 'Add New Rent', 'text-domain' ),
			'edit_item'           => __( 'Edit Rent', 'text-domain' ),
			'new_item'            => __( 'New Rent', 'text-domain' ),
			'view_item'           => __( 'View Rent', 'text-domain' ),
			'search_items'        => __( 'Search Rent', 'text-domain' ),
			'not_found'           => __( 'No Rent found', 'text-domain' ),
			'not_found_in_trash'  => __( 'No Rent found in Trash', 'text-domain' ),
			'parent_item_colon'   => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'           => __( 'الإيجار', 'text-domain' ),
		);
	
		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => null,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title', 'editor', 'author', 'thumbnail',
				'excerpt','custom-fields', 'trackbacks', 'comments',
				'revisions', 'page-attributes', 'post-formats'
				)
		);
	
		register_post_type( 'Rent', $args );
	}



	/**
	* Registers a new post type
	* @uses $wp_post_types Inserts new post type object into the list
	*
	* @param string  Post type key, must not exceed 20 characters
	* @param array|string  See optional args description above.
	* @return object|WP_Error the registered post type object, or an error object
	*/
	function rent_register_sell() {
	
		$labels = array(
			'name'                => __( 'Sell', 'text-domain' ),
			'singular_name'       => __( 'Sell', 'text-domain' ),
			'add_new'             => _x( 'Add New Sell', 'text-domain', 'text-domain' ),
			'add_new_item'        => __( 'Add New Sell', 'text-domain' ),
			'edit_item'           => __( 'Edit Sell', 'text-domain' ),
			'new_item'            => __( 'New Sell', 'text-domain' ),
			'view_item'           => __( 'View Sell', 'text-domain' ),
			'search_items'        => __( 'Search Sell', 'text-domain' ),
			'not_found'           => __( 'No Sell found', 'text-domain' ),
			'not_found_in_trash'  => __( 'No Sell found in Trash', 'text-domain' ),
			'parent_item_colon'   => __( 'Parent Sell:', 'text-domain' ),
			'menu_name'           => __( 'البيع', 'text-domain' ),
		);
	
		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => null,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title', 'editor', 'author', 'thumbnail',
				'excerpt','custom-fields', 'trackbacks', 'comments',
				'revisions', 'page-attributes', 'post-formats'
				)
		);
	
		register_post_type( 'Sell', $args );
	}

		/**
	* Registers a new post type
	* @uses $wp_post_types Inserts new post type object into the list
	*
	* @param string  Post type key, must not exceed 20 characters
	* @param array|string  See optional args description above.
	* @return object|WP_Error the registered post type object, or an error object
	*/
	function rent_register_pay() {
	
		$labels = array(
			'name'                => __( 'Payment', 'text-domain' ),
			'singular_name'       => __( 'Payment', 'text-domain' ),
			'add_new'             => _x( 'Add New Payment', 'text-domain', 'text-domain' ),
			'add_new_item'        => __( 'Add New Payment', 'text-domain' ),
			'edit_item'           => __( 'Edit Payment', 'text-domain' ),
			'new_item'            => __( 'New Payment', 'text-domain' ),
			'view_item'           => __( 'View Payment', 'text-domain' ),
			'search_items'        => __( 'Search Payment', 'text-domain' ),
			'not_found'           => __( 'No Payment found', 'text-domain' ),
			'not_found_in_trash'  => __( 'No Payment found in Trash', 'text-domain' ),
			'parent_item_colon'   => __( 'Parent Payment:', 'text-domain' ),
			'menu_name'           => __( 'المصاريف', 'text-domain' ),
		);
	
		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => null,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title', 'editor', 'author', 'thumbnail',
				'excerpt','custom-fields', 'trackbacks', 'comments',
				'revisions', 'page-attributes', 'post-formats'
				)
		);
	
		register_post_type( 'Payment', $args );
	}

};
$rent_custom_posts = new rent_custom_posts;